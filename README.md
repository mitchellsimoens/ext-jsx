# ext-jsx

The purpose of this is to parse JSX syntax to create [Ext JS](https://www.sencha.com/products/extjs/) compliant
classes and configuration objects. This module is very experimental, this came from a hack session
as a proof of concept.

## JSX

[JSX](https://facebook.github.io/jsx/) is a spec from Facebook and is used in [React](https://facebook.github.io/react/)
development. JSX has been touted as one of the key features in ease of developing React applications. This ease
of developing is why this module exists, to make Ext JS a little more approachable.

## Get Started

Please see the [`example`](example) directory.

## Create Components

To create a component, you use an XML like structure with the tag name set to a descriptor (more on that later).
A simple example on creating a component would be:

    var component = (
        <component width='800' height='600'>
            Hello there!
        </component>
    );

This would get transpiled to:

    var component = {
        width  : 800,
        height : 600,
        html   : 'Hello there!',
        xtype  : 'component'
    };

A larger example with a layout:

    var container = (
        <container>
            <layout type='hbox' align='stretch' />
            <items>
                <component flex='1'>Left</component>
                <component flex='2'>right</component>
            </items>
        </container>
    );

Would get transpiled to:

    var container = {
        layout : {
            type  : 'hbox',
            align : 'stretch
        },
        items  : [
            {
                flex  : 1,
                html  : 'Left',
                xtype : 'component'
            },
            {
                flex  : 2,
                html  : 'Right',
                xtype : 'component'
            }
        ],
        xtype  : 'container'
    };

A grid example:

    var grid = (
        <gridpanel title='Users'>
            <store>
                <field name='firstName' />
                <field name='lastName' />
                <field name='email' />

                <data firstName='Mitchell' lastName='Simoens' email='mitchell.simoens@sencha.com' />
                <data firstName='Mike' lastName='Estes' email='mike.estes@sencha.com' />
                <data firstName='Craig' lastName='Gering' email='craig@sencha.com' />
            </store>

            <plugin type='cellediting' clicksToEdit='1' />

            <selModel type='rowmodel' mode='SIMPLE' />

            <column text='First Name' dataIndex='firstName' flex='1' editor='textfield' />
            <column text='Last Name' dataIndex='lastName' flex='1' editor='textfield' />
            <column text='Email' dataIndex='email' flex='1'>
                <editor xtype='textfield' allowBlank='false' vtype='email' />
            </column>
        </gridpanel>
    );

would get transpiled to:

    var grid = {
        title    : 'Users',
        store    : {
            fields : [
                {
                    name : 'firstName'
                },
                {
                    name : 'lastName'
                },
                {
                    name : 'email'
                }
            ],
            data   : [
                {
                    firstName : 'Mitchell',
                    lastName  : 'Simoens',
                    email     : 'mitchell.simoens@sencha.com'
                },
                {
                    firstName : 'Mike',
                    lastName  : 'Estes',
                    email     : 'mike.estes@sencha.com'
                },
                {
                    firstName : 'Craig',
                    lastName  : 'Gering',
                    email     : 'craig@sencha.com'
                }
            ]
        },
        plugins  : [
            {
                type         : 'cellediting',
                clicksToEdit : 1
            }
        ],
        selModel : {
             type : 'rowmodel',
             mode : 'SIMPLE'
        },
        columns  : [
            {
                text      : 'First Name',
                dataIndex : 'firstName',
                flex      : 1,
                editor    : 'textfield',
                xtype     : 'column'
            },
            {
                text      : 'Last Name',
                dataIndex : 'lastName',
                flex      : 1,
                editor    : 'textfield',
                xtype     : 'column'
            },
            {
                text      : 'Email',
                dataIndex : 'email',
                flex      : 1,
                editor    : {
                    xtype      : 'textfield',
                    allowBlank : false,
                    vtype      : 'email'
                },
                xtype     : 'column'
            }
        ],
        xtype    : 'gridpanel'
    };

## Really creating an instance

In the last section, we created configuration objects but what if you want to create an actual instance?
There are two ways to do this and depends on what class you want to create. If you want to create an Ext JS
component, then you can use `Ext.create` with the JSX:

    Ext.create(
        (
            <container>
                <layout type='hbox' align='stretch' />
                <items>
                    <component flex='1'>Left</component>
                    <component flex='2'>right</component>
                </items>
            </container>
        )
    );

and this would get transpiled into:

    Ext.create({
        layout : {
            type  : 'hbox',
            align : 'stretch
        },
        items  : [
            {
                flex  : 1,
                html  : 'Left',
                xtype : 'component'
            },
            {
                flex  : 2,
                html  : 'Right',
                xtype : 'component'
            }
        ],
        xtype  : 'container'
    });

If you have your own subclass that you used JSX for (see the next section on how to define classes), then
you can set a variable and have the JSX transpile using it.

    const MyComponent = require('./app/view/Main.jsx);

    (
        <MyComponent />
    );

Notice the tag name matches the `MyComponent` constant variable we created, the tag will use that variable.

### Why the difference?

At this point, Ext JS isn't setup to work with Ext JSX since Ext JSX is just a result of a late night idea.
However, if a class was created with Ext JSX then you can use the constant variable means which is preferred.

## Defining Classes

To define a class, you must use the `define` tag:

    (
        <define name="MyClassName" extend="Ext.container.Container" xtype="myclassname">
            <layout type="border" />
            <items>
                <component region="center">Hello there</component>
            </items>
        </define>
    );

This will get transpiled to:

    Ext.define('MyClassName', {
        extend : 'Ext.container.Container',
        xtype  : 'myclassname',

        layout : {
            type : 'border'
        },

        items : [
            {
                region : 'center',
                html   : 'Hello there',
                xtype  : 'component'
            }
        ]
    });

## Defining Custom Parser

In JSX, the element name (or tag name) should match to a parser or the default parser will be used.
For example, `<component>` will use the Component parser where `<custom>` will use the BaseParser
because there isn't one defined to match that name. The default parser may be enough but when you
need to do extra parsing of children or attributes then you will need to define your own parser.

Here is an example parser:

    const BaseParser = require('ext-jsx/parser/BaseParser');
    const ExtJSX     = require('ext-jsx');

    class Custom extends BaseParser {
        static get name () {
            return 'custom';
        }
    }

    ExtJSX.register(Custom);

The static name getter is important, this is what tells Ext JSX what tag this parser matches. There
are important methods that can be overridden:

 - `parseChild` This method will accept the child object and current config of the element. Here,
    you can do special things like take over what property onto the current config to add the child to.
 - `parseAttribute` This method applies the attributes onto the current config. You can do special
    parsing of the attributes. By default, ints and bools will be parsed.
 - `get out` The out getter is what is used to take the current config object and return the Ext JS
    compliant object.
