'use strict';

const Base       = require('sencha-core/Base');
const BaseParser = require('./parser/BaseParser');

class ExtJSX extends Base {
    static get meta () {
        return {
            prototype : {
                isExtJSX : true,

                parsers : {}
            }
        };
    }

    register (parser) {
        let parsers = [...arguments];

        parsers.forEach((parser) => {
            if (typeof parser === 'string') {
                parser = require(parser);
            }

            this.parsers[parser.name] = parser;
        });
    }

    getParser (name, jsx) {
        let parsers = this.parsers,
            parser;

        if (name) {
            if (typeof name === 'string') {
                parser = parsers[name];

                if (!parser) {
                    if (name === 'define') {
                        let extend = jsx.attributes.extend;

                        if (extend) {
                            let temp = Ext.ClassManager.get(extend);

                            if (temp) {
                                name = temp;
                            } else {
                                console.warn('Class not found:', extend);
                            }
                        }
                    }
                }
            }

            if (name.$isClass) {
                let cls = name;

                while (cls && !parser) {
                    let xtype = cls.xtype;

                    if (xtype) {
                        parser = parsers[xtype];
                    }

                    cls = cls.superclass;
                }
            } else {
                parser = parsers[name];
            }
        }

        if (!parser) {
            parser = BaseParser;
        }

        return parser;
    }

    transform (jsx) {
        if (typeof jsx === 'object') {
            let parser = this.getParser(jsx.elementName, jsx);

            return parser.transform(jsx);
        } else {
            return jsx;
        }
    }
}

module.exports = new ExtJSX();
