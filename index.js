'use strict';

const ExtJSX = require('./ExtJSX');

ExtJSX.register(
    require('./parser/DockedItems'),
    require('./parser/Items'),
    require('./parser/Layout'),
    require('./parser/ext/container/Container'),
    require('./parser/ext/data/Store'),
    require('./parser/ext/grid/column/Column'),
    require('./parser/ext/grid/Panel'),
    require('./parser/ext/panel/Panel'),
    require('./parser/ext/toolbar/Toolbar'),
    require('./parser/ext/Component')
);

module.exports = function(jsx) {
    return ExtJSX.transform(jsx);
};

module.register = function(parser) {
    ExtJSX.register(parser);
};
