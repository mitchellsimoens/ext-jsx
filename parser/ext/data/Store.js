'use strict';

const Base = require('../../BaseParser');

class Store extends Base {
    static get name () {
        return 'store';
    }

    parseChild (child, config = this.cfg) {
        if (child) {
            let typeProp = this.typeProp,
                type     = child[typeProp] || child.xtype;

            if (type) {
                if (type === 'field') {
                    if (config.fields) {
                        config.fields.push(child);
                    } else {
                        config.fields = [child];
                    }

                    delete child[typeProp];
                } else if (type === 'data') {
                    if (config.data) {
                        config.data.push(child);
                    } else {
                        config.data = [child];
                    }

                    delete child[typeProp];
                }
            }
        }

        super.parseChild(child, config);
    }
}

module.exports = Store;
