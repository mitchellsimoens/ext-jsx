'use strict';

const Container = require('../container/Container');

class Toolbar extends Container {
    static get name () {
        return 'toolbar';
    }

    parseChild (child, config = this.cfg) {
        if (typeof child === 'object') {
            delete child[this.typeProp];

            if (config.items) {
                config.items.push(child);
            } else {
                config.items = [child];
            }
        }

        super.parseChild(child, config);
    }
}

module.exports = Toolbar;
