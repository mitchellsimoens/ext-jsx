'use strict';

const Component = require('../../Component');

class Column extends Component {
    static get name () {
        return 'column';
    }

    static get columns () {
        return {
            boolean  : 1,
            check    : 1,
            date     : 1,
            number   : 1,
            template : 1,
            widget   : 1
        };
    }

    static parseConfig (config) {
        let type  = config.type,
            xtype = 'gridcolumn';

        if (type) {
            let columns    = this.columns,
                recognized = columns[type];

            if (recognized) {
                if (typeof recognized === 'string') {
                    xtype = recognized;
                } else {
                    xtype = type + 'column';
                }
            }
        }

        config.xtype = xtype;

        return config;
    }

    parseChild (child, config = this.cfg) {
        if (child) {
            let typeProp = this.typeProp,
                type     = child[typeProp] || child.xtype;

            if (type) {
                if (type === 'editor') {
                    config.editor = child;
                } else if (type === 'column') {
                    child = this.constructor.parseConfig(child);

                    if (config.columns) {
                        config.columns.push(child);
                    } else {
                        config.columns = [child];
                    }
                } else if (type === 'widget') {
                    config[type] = child;
                }

                delete child[typeProp];
            }
        }
    }
}

module.exports = Column;
