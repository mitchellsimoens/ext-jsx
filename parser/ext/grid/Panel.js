'use strict';

const Column = require('./column/Column');
const Panel  = require('../panel/Panel');

class Grid extends Panel {
    static get name () {
        return 'gridpanel';
    }

    parseChild (child, config = this.cfg) {
        if (child) {
            let typeProp = this.typeProp,
                type     = child[typeProp] || child.xtype;

            if (type) {
                if (type === 'column') {
                    child = Column.parseConfig(child);

                    if (config.columns) {
                        config.columns.push(child);
                    } else {
                        config.columns = [child];
                    }
                } else if (type === 'selModel') {
                    delete child[typeProp];

                    config.selModel = child;
                }
            }
        }

        super.parseChild(child, config);
    }
}

module.exports = Grid;
