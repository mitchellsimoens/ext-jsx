'use strict';

const Component = require('../Component');

class Container extends Component {
    static get name () {
        return 'container';
    }

    parseChild (child, config = this.cfg) {
        let typeProp = this.typeProp;

        if (child) {
            if (child[typeProp] === 'items') {
                child.items = child.items.map((child) => {
                    if (typeof child === 'string') {
                        child = child.trim();

                        if (child) {
                            child = {
                                xtype : 'component',
                                html  : child
                            };
                        }
                    }

                    return child;
                });

                config.items = child = child.items;
            }
        }

        super.parseChild(child, config);
    }
}

module.exports = Container;
