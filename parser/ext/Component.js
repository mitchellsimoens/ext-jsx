'use strict';

const Base = require('../BaseParser');

class Component extends Base {
    static get name () {
        return 'component';
    }

    static get propToProp () {
        return {
            isViewController : 'controller'
        };
    }

    beforeOut (config) {
        let typeProp = this.typeProp,
            xtype    = config[typeProp];

        if (typeof xtype === 'string' && xtype !== 'define') {
            config.xtype = xtype;

            delete config[typeProp];
        }
    }

    parseChild (child, config = this.cfg) {
        let typeProp = this.typeProp;

        if (child) {
            if (child.isInstance) {
                let propToProp = this.constructor.propToProp;

                for (let prop in propToProp) {
                    if (child[prop]) {
                        config[propToProp[prop]] = child;

                        break;
                    }
                }
            } else if (typeof child === 'string') {
                child = child.trim();

                if (child) {
                    config.xtype = 'component';
                    config.html  = child;

                    child = null;
                }
            }
        }

        super.parseChild(child, config);
    }
}

module.exports = Component;
