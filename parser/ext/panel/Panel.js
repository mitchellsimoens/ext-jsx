'use strict';

const Container = require('../container/Container');

class Panel extends Container {
    static get name () {
        return 'panel';
    }

    parseChild (child, config = this.cfg) {
        if (child) {
            let typeProp = this.typeProp,
                type     = child[typeProp] || child.xtype;

            if (type) {
                if (type === 'dockedItems') {
                    config.dockedItems = child = child.items.map(item => {
                        if (!item.xtype) {
                            item.xtype = item[typeProp];

                            delete item[typeProp];
                        }

                        return item;
                    });
                }
            }
        }

        super.parseChild(child, config);
    }
}

module.exports = Panel;
