'use strict';

const Base = require('./BaseParser');

class Layout extends Base {
    static get name () {
        return 'layout';
    }
}

module.exports = Layout;
