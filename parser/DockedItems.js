'use strict';

const Base = require('./BaseParser');

class DockedItems extends Base {
    static get name () {
        return 'dockedItems';
    }

    parseChild (child, config = this.cfg) {
        if (typeof child === 'string') {
            child = child.trim();
        }

        if (child) {
            if (config.items) {
                config.items.push(child);
            } else {
                config.items = [child];
            }
        }
    }
}

module.exports = DockedItems;
