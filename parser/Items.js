'use strict';

const Base = require('./BaseParser');

class Items extends Base {
    static get name () {
        return 'items';
    }

    parseChild (child, config = this.cfg) {
        if (typeof child === 'string') {
            child = child.trim();
        }

        if (child) {
            if (config.items) {
                config.items.push(child);
            } else {
                config.items = [child];
            }
        }
    }
}

module.exports = Items;
