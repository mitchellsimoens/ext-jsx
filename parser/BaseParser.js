'use strict';

const Base   = require('sencha-core/Base');
const ExtJSX = require('../ExtJSX');

const numRe  = /^[\d,.]+$/;
const boolRe = /^true|false$/;

class BaseParser extends Base {
    static get meta () {
        return {
            prototype : {
                isExtJSXParser : true,
                typeProp       : '$ExtType'
            }
        };
    }

    static transform (jsx) {
        let instance = new this(jsx);

        return instance.out;
    }

    ctor () {
        let me = this;

        me.cfg = {
            [me.typeProp] : me.elementName
        };

        me.parseAttributes();
        me.parseChildren();
    }

    get out () {
        let config = this.cfg;

        if (this.beforeOut) {
            this.beforeOut(config);
        }

        let typeProp = this.typeProp,
            cls      = config[typeProp];

        if (cls) {
            if (cls.$isClass) {
                delete config[typeProp];

                return new cls(config);
            } else if (cls.isInstance) {
                return cls;
            } else if (cls === 'define') {
                let name = config.name;

                delete config[typeProp];
                delete config.name;

                return Ext.define(name, config);
            }
        }

        return config;
    }

    parseAttributes (attributes = this.attributes, config = this.cfg) {
        if (attributes) {
            for (let cfg in attributes) {
                let value = this.parseAttribute(cfg, attributes[cfg], config);

                if (value != null) {
                    attributes[cfg] = config[cfg] = value;
                }
            }
        }
    }

    parseAttribute(name, value, config = this.cfg) {
        return value;
    }

    parseChildren (children = this.children, config = this.cfg) {
        if (children) {
            children.forEach((child) => this.parseChild(child));
        }
    }

    parseChild (child, config = this.cfg) {
        if (child) {
            let typeProp = this.typeProp,
                type     = child[typeProp];

            if (type) {
                if (type === 'plugin') {
                    if (config.plugins) {
                        config.plugins.push(child);
                    } else {
                        config.plugins = [child];
                    }
                } else if (type === 'listener') {
                    if (config.listeners) {
                        config.listeners[child.event] = child.fn;
                    } else {
                        config.listeners = {
                            [child.event] : child.fn
                        };
                    }
                } else {
                    config[type] = child;
                }

                delete child[typeProp];
            }
        }
    }
}

module.exports = BaseParser;
