module.exports = function(grunt) {
    var webpack = require('webpack');

    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),

        exec : {
            clean : {
                cwd : '',
                cmd : 'git clean -fxd'
            },

            npm : {
                cwd : '',
                cmd : 'npm install'
            }
        },

        webpack : {
            options : {
                stats : {
                    colors  : true,
                    modules : true,
                    reasons : true
                },

                progress    : true,
                failOnError : true,
                devtool     : 'source-map',

                keepAlive : false,
                watch     : false,

                target : 'node',

                module : {
                    preLoaders : [
                        {
                            test    : /\.js$/,
                            exclude : /node_modules/,
                            loader  : 'jshint-loader'
                        }
                    ],
                    loaders : [
                        {
                            test    : /\.jsx?$/,
                            exclude : /node_modules/,
                            loader  : 'babel?presets[]=es2015'
                        }
                    ]
                },
                plugins : [
                    new webpack.IgnorePlugin(/\.(json|md)$/)
                ]
            },

            build : {
                entry  : {
                    index : './src/index.jsx'
                },
                output : {
                    path              : 'public/js/',
                    filename          : '[name]-bundle.js',
                    sourceMapFilename : '[file].map'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-webpack');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('default', [
        'webpack'
    ]);

    grunt.registerTask('clean', [
        'exec:clean',
        'exec:npm'
    ]);
};
