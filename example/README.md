# ext-jsx-example

This is the example I use to test Ext JSX on. It's not a full Ext JS application, just a grid with some stuff in it. As I add features, this example will become more complex.

## Get running

First, we need to install Babel and Grunt globally:

    npm install -g babel-cli grunt-cli

Next, we need to install the local dependencies:

    npm install

Last, we need to build the example:

    grunt

Now you can load the example in your browser: http://localhost/ext-jsx/example/

You should now see a grid with some columns and stuff.

## Important files

There are some important files in this example (do not forget `.babelrc`):

 - `.babelrc` This is a config file for babel which will allow you to use the ext-jsx plugin. **DO NOT FORGET THIS FILE OR ELSE THE BUILD WILL NOT WORK! IT MAY BE HIDDEN DEPENDING ON YOUR OS AND SETTINGS.**
 - `Gruntfile.js` You can use grunt to build everything into a single JS file. This also uses webpack.
 - `src/` This is the source of your application. This is what will get transformed from JSX into JavaScript.
 - `public/` This the JavaScript taken from the `src/` directory and it's dependencies. This is what should be deployed. The JavaScript will actually go into the `public/js/` directory that will be created for you.
