'use strict';

const MyGrid = require('./MyGrid.jsx');

const onFoo = function() {
    console.log('onFoo on grid fired');
};

Ext.onReady(function() {
    (
        <MyGrid title="Users" height={400} width={1000} renderTo={Ext.getBody()}>
            <store>
                <field name="firstName" />
                <field name="lastName" />
                <field name="email" />
                <field name="active" type="boolean" />
                <field name="join" type="date" dateFormat="Y-m-d" />
                <field name="ideas" type="int" />
                <field name="complete" type="number" />

                <data
                    firstName="Mitchell"
                    lastName="Simoens"
                    email="mitchell.simoens@sencha.com"
                    active={true}
                    join="2016-01-01"
                    ideas={100}
                    complete={1}
                />
                <data
                    firstName="Mike"
                    lastName="Estes"
                    email="mike.estes@sencha.com"
                    active={true}
                    join="2016-02-15"
                    ideas={90}
                    complete={0.9}
                />
                <data
                    firstName="Craig"
                    lastName="Gering"
                    email="craig@sencha.com"
                    active={false}
                    join="2016-03-31"
                    ideas={1}
                    complete={0.01}
                />
            </store>

            <listener event="itemclick" fn={onFoo} />
        </MyGrid>
    );
});
