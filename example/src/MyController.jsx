'use strict';

const obj = {
    onAdd : function() {
        let grid = this.getView();

        Ext.Msg.prompt(
            'Enter Name',
            'Please enter a name: {firstname} {lastname}',
            function(btn, name) {
                if (btn === 'ok') {
                    let store = grid.getStore();

                    name = name.split(' ');

                    store.add({
                        firstName : name[0],
                        lastName  : name[1]
                    });
                }
            }
        );
    },

    onBeforeSelect : function(selModel, record) {
        return !record.get('active');
    },

    onClick : function() {
        console.log('onClick on view controller');
    }
};

module.exports = (
    <define
        name="MyController"
        extend="Ext.app.ViewController"
        alias="controller.mycontroller"
        {...obj}
    />
);
