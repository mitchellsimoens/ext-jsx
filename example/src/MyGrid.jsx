const MyController = require('./MyController.jsx');

module.exports = (
    <define name="MyGrid" extend="Ext.grid.Panel" xtype="mygrid">
        <MyController />

        <plugin ptype="cellediting" clicksToEdit={1} />

        <selModel type="rowmodel" mode="SIMPLE" />

        <listener event="beforeselect" fn="onBeforeSelect" />
        <listener event="itemclick" fn="onClick" />

        <dockedItems>
            <toolbar dock="top">
                <button text="Add" handler="onAdd" />
            </toolbar>
        </dockedItems>

        <column type="boolean" text="Active" dataIndex="active" width={60} trueText="Yes" falseText="No" />
        <column type="check" text="Active" dataIndex="active" width={60} />

        <column text="Name">
            <column text="First Name" dataIndex="firstName" width={100} editor="textfield" />
            <column text="Last Name" dataIndex="lastName" width={100} editor="textfield" />
        </column>

        <column type="template" text="Display Name" width={150} tpl="{lastName}, {firstName}" />
        <column text="Email" dataIndex="email" flex={1}>
            <editor xtype="textfield" allowBlank="false" vtype="email" />
        </column>

        <column type="date" text="Join Date" dataIndex="join" width={100} format="Y-m-d" />
        <column type="number" text="Ideas" dataIndex="ideas" width={60} format="0,000" />

        <column type="widget" text="Complete" dataIndex="complete" width={150}>
            <widget xtype="progressbarwidget" textTpl="{percent:number('0')}% completeness" />
        </column>
    </define>
);
